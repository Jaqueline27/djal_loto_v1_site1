import { RouterModule, Routes} from '@angular/router';

import { NosotrosComponent} from "./nosotros/nosotros.component";
import { ActividadesComponent} from "./actividades/actividades.component";
import { FundacionComponent} from "./fundacion/fundacion.component";
const app_routes: Routes =  [
    { path: 'nosotros', component: NosotrosComponent },
    { path: 'actividades', component: ActividadesComponent },
    { path: 'fundacion', component: FundacionComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'app'}

];

export const app_routing = RouterModule.forRoot(app_routes);
