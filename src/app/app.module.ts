import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//rutas
import {app_routing} from "./app.routes";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { FundacionComponent } from './fundacion/fundacion.component';





@NgModule({
  declarations: [
    AppComponent,
    NosotrosComponent,
    ActividadesComponent,
    FundacionComponent,
   
    
    

    
    
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    app_routing

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
